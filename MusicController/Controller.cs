﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CSCore.CoreAudioAPI;

namespace MusicControls
{
    public class Controller
    {
        public const int KEYEVENTF_EXTENTEDKEY = 1;
        public const int KEYEVENTF_KEYUP = 0;
        public const int VK_MEDIA_NEXT_TRACK = (byte)Keys.MediaNextTrack;
        public const int VK_MEDIA_PLAY_PAUSE = (byte)Keys.MediaPlayPause;
        public const int VK_MEDIA_PREV_TRACK = (byte)Keys.MediaPreviousTrack;
        public const int VK_MEDIA_VOL_UP = (byte)Keys.VolumeUp;
        public const int VK_MEDIA_VOL_DOWN = (byte)Keys.VolumeDown;
        public const int VK_MEDIA_MUTE = (byte)Keys.VolumeMute;

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);

        public MMDevice GetDefaultRenderDevice()
        {
            using (var enumerator = new MMDeviceEnumerator())
            {
                return enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);
            }
        }

        public bool IsAudioPlaying(MMDevice device)
        {
            using (var meter = AudioMeterInformation.FromDevice(device))
            {
                return meter.PeakValue > 0;
            }
        }

        public void PlayPause()
        {
            keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
        }

        public void Prev()
        {
            keybd_event(VK_MEDIA_PREV_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
        }

        public void Next()
        {
            keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
        }

        public void VolUp()
        {
            keybd_event(VK_MEDIA_VOL_UP, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
        }

        public void VolDown()
        {
            keybd_event(VK_MEDIA_VOL_DOWN, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
        }
        
        public void Mute()
        {
            keybd_event(VK_MEDIA_MUTE, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
        }
    }
}
