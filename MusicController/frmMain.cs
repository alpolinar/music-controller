﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HotKeysLib;

namespace MusicControls
{
    public partial class frmMain : Form
    {
        private Controller controller = new Controller();
        private HotKeysLib.GlobalHotKeys ghkPrev;
        private HotKeysLib.GlobalHotKeys ghkNext;
        private HotKeysLib.GlobalHotKeys ghkVolUp;
        private HotKeysLib.GlobalHotKeys ghkVolDown;
        private HotKeysLib.GlobalHotKeys ghkPlayPause;
        private HotKeysLib.GlobalHotKeys ghkVolMute;

        //Loads the default hotkeys
        public frmMain()
        {
            InitializeComponent();
            LoadDefaultHotkeys();
        }
        //Sets the keys to register
        private void LoadDefaultHotkeys()
        {
            ghkPrev = new HotKeysLib.GlobalHotKeys(Constants.ALT + Constants.SHIFT, Keys.Left, this);
            ghkNext = new HotKeysLib.GlobalHotKeys(Constants.ALT + Constants.SHIFT, Keys.Right, this);
            ghkVolUp = new HotKeysLib.GlobalHotKeys(Constants.ALT + Constants.SHIFT, Keys.Up, this);
            ghkVolDown = new HotKeysLib.GlobalHotKeys(Constants.ALT + Constants.SHIFT, Keys.Down, this);
            ghkPlayPause = new HotKeysLib.GlobalHotKeys(Constants.ALT + Constants.SHIFT, Keys.Space, this);
            ghkVolMute = new HotKeysLib.GlobalHotKeys(Constants.ALT + Constants.SHIFT, Keys.M, this);
        }
        //Registers the keys on load
        private void frmMain_Load(object sender, EventArgs e)
        {
            if (ghkPrev.Register() && ghkNext.Register() && ghkVolUp.Register() && ghkVolDown.Register() && ghkPlayPause.Register() && ghkVolMute.Register())
                Console.WriteLine("Registered");
            else
                Console.WriteLine("Failed");
        }
        //Triggers functions based on which key is pressed
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == HotKeysLib.Constants.WM_HOTKEY_MSG_ID) {
                switch (GetKey(m.LParam)){
                    case Keys.Left:
                        controller.Prev();
                        break;
                    case Keys.Right:
                        controller.Next();
                        break;
                    case Keys.Up:
                        controller.VolUp();
                        break;
                    case Keys.Down:
                        controller.VolDown();
                        break;
                    case Keys.Space:
                        controller.PlayPause();
                        break;
                    case Keys.M:
                        controller.Mute();
                        break;
                }
            }
            base.WndProc(ref m);
        }
        //Checks which key is pressed
        private Keys GetKey(IntPtr LParam)
        {
            return (Keys)((LParam.ToInt32()) >> 16);
        }

        //Load the Previous song
        private void btnPrev_Click(object sender, EventArgs e)
        {
            controller.Prev();
        }
        //Play or Pause the song
        private void btnPlayPause_Click(object sender, EventArgs e)
        {
            controller.PlayPause();
            Console.WriteLine(controller.GetDefaultRenderDevice());
            if (controller.IsAudioPlaying(controller.GetDefaultRenderDevice()))
                Console.WriteLine("playing"); 
            else
                Console.WriteLine("stopped"); 
        }
        //Load the Next song
        private void btnNext_Click(object sender, EventArgs e)
        {
            controller.Next();
        }
        //Increase the volume
        private void btnVolUp_Click(object sender, EventArgs e)
        {
            controller.VolUp();
        }
        //Decrease the volume
        private void btnVolDown_Click(object sender, EventArgs e)
        {
            controller.VolDown();
        }
        //Mute the audioi
        private void btnMute_Click(object sender, EventArgs e)
        {
            controller.Mute();
        }
        //Minimize application
        private void frmMain_Resize(object sender, EventArgs e)
        {
            MinimizeToTray();
        }
        //Open the app on double click from system tray
        private void nicMinimized_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            nicMinimized.Visible = false;
        }
        //Minimize the app to icon tray
        private void MinimizeToTray()
        {
            bool cursorNotInBar = Screen.GetWorkingArea(this).Contains(Cursor.Position); //checks if the cursor is on the windows bar
            if (this.WindowState == FormWindowState.Minimized && cursorNotInBar)
            {
                this.ShowInTaskbar = false;
                nicMinimized.Visible = true;
                this.Hide();
            }
        }
        //Close the applciation using the menu strip
        private void msQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void msReadMe_Click(object sender, EventArgs e)
        {
            //string fileName = Path.GetDirectoryName(Application.LocalUserAppDataPath)+"\\readme.txt";
            //Console.WriteLine(fileName);
            //System.Diagnostics.Process.Start("notepad.exe", fileName);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!ghkPrev.Unregiser() && !ghkNext.Unregiser() && !ghkVolUp.Unregiser() && !ghkVolDown.Unregiser() && !ghkPlayPause.Unregiser() && ghkVolMute.Unregiser())
                MessageBox.Show("Hotkey failed to unregister!");
        }

        ////Displays a context menu on right click
        //private void btnPrev_MouseUp(object sender, MouseEventArgs e)
        //{
        //    switch (e.Button)
        //    {
        //        case MouseButtons.Right:
        //            ContextMenu cm = new ContextMenu();
        //            cm.MenuItems.Add("Add Hotkey", AddPrevHotkey);
        //            cm.MenuItems.Add("Remove Hotkey", RemovePrevHotkey);
        //            btnPrev.ContextMenu = cm;
        //            break;
        //    }
        //}
        ////Adds a Hotkey to the button
        //private void AddPrevHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Add hotkey");

        //}
        ////Removes the Hotkey of the button
        //private void RemovePrevHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Remove hotkey");
        //}
        ////Displays a context menu on right click
        //private void btnPlayPause_MouseUp(object sender, MouseEventArgs e)
        //{
        //    switch (e.Button)
        //    {
        //        case MouseButtons.Right:
        //            ContextMenu cm = new ContextMenu();
        //            cm.MenuItems.Add("Add Hotkey", AddPlayPauseHotkey);
        //            cm.MenuItems.Add("Remove Hotkey", RemovePlayPauseHotkey);
        //            btnPlayPause.ContextMenu = cm;
        //            break;
        //    }
        //}
        ////Adds a Hotkey to the button
        //private void AddPlayPauseHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Add hotkey");
        //}
        ////Removes the Hotkey of the button
        //private void RemovePlayPauseHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Remove hotkey");
        //}
        ////Displays a context menu on right click
        //private void btnNext_MouseUp(object sender, MouseEventArgs e)
        //{
        //    switch (e.Button)
        //    {
        //        case MouseButtons.Right:
        //            ContextMenu cm = new ContextMenu();
        //            cm.MenuItems.Add("Add Hotkey", AddNextHotkey);
        //            cm.MenuItems.Add("Remove Hotkey", RemoveNextHotkey);
        //            btnNext.ContextMenu = cm;
        //            break;
        //    }
        //}
        ////Adds a Hotkey to the button
        //private void AddNextHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Add hotkey");
        //}
        ////Removes the Hotkey of the button
        //private void RemoveNextHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Remove hotkey");
        //}
        ////Displays a context menu on right click
        //private void btnVolDown_MouseUp(object sender, MouseEventArgs e)
        //{
        //    switch (e.Button)
        //    {
        //        case MouseButtons.Right:
        //            ContextMenu cm = new ContextMenu();
        //            cm.MenuItems.Add("Add Hotkey", AddVolDownHotkey);
        //            cm.MenuItems.Add("Remove Hotkey", RemoveVolDownHotkey);
        //            btnVolDown.ContextMenu = cm;
        //            break;
        //    }
        //}
        ////Adds a Hotkey to the button
        //private void AddVolDownHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Add hotkey");
        //}
        ////Removes the Hotkey of the button
        //private void RemoveVolDownHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Remove hotkey");
        //}
        ////Displays a context menu on right click
        //private void btnVolUp_MouseUp(object sender, MouseEventArgs e)
        //{
        //    switch (e.Button)
        //    {
        //        case MouseButtons.Right:
        //            ContextMenu cm = new ContextMenu();
        //            cm.MenuItems.Add("Add Hotkey", AddVolUpHotkey);
        //            cm.MenuItems.Add("Remove Hotkey", RemoveVolUpHotkey);
        //            btnVolUp.ContextMenu = cm;
        //            break;
        //    }
        //}
        ////Adds a Hotkey to the button
        //private void AddVolUpHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Add hotkey");
        //}
        ////Removes the Hotkey of the button
        //private void RemoveVolUpHotkey(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Remove hotkey");
        //}
    }
}
